<?php
$string['pluginname']	=	'CU Attendance';
$string['allusers']		=	'All';
$string['contributions'] = 	'Contributions';
$string['cuattendance:view'] = 'View CU Attendance Report';
$string['lastattendance'] = 	'Last Date of Attendance (within date range)';
$string['lastcourseaccess'] =	'Last Access to Course (all-time)';
$string['lastmdlaccess'] = 'Last Access to Moodle';
$string['logdetails'] =	'Log Details';
$string['pagetitle'] = 'Student Attendance Report';
