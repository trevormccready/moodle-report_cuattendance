<?php
/**
* @package    report_cuattendance
* @copyright  2016 Cornerstone University {@link http://www.cornerstone.edu}
* @author	  Trevor McCready
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

defined('MOODLE_INTERNAL') || die;


/**
 * Callback to verify if the given instance of store is supported by this report or not.
 *
 * @param string $instance store instance.
 *
 * @return bool returns true if the store is supported by the report, false otherwise.
 */
function report_cuattendance_supports_logstore($instance) {
	// Use '\core\log\sql_select_reader' instead of '\core\log\sql_reader' in Moodle 2.7 and Moodle 2.8.
	if ($instance instanceof \core\log\sql_reader) {
		return true;
	}
	return false;
}

/**
 * Function to add link to Course Navigation under Reports
 * 
 * @param unknown $navigation
 * @param unknown $course
 * @param unknown $context
 */
function report_cuattendance_extend_navigation_course($navigation, $course, $context) {
	if (has_capability('report/cuattendance:view', $context)) {
		$url = new moodle_url('/report/cuattendance/index.php', array('id'=>$course->id));
		$navigation->add(get_string('pluginname', 'report_cuattendance'), $url, navigation_node::TYPE_SETTING, null, null, new pix_icon('i/report', ''));
	}
}

function report_cuattendance_plugin_exists($plugin) {
    global $DB;

    $plugin_exists = $DB->get_record('config_plugins',array('plugin'=>$plugin,'name'=>'version'));
    if($plugin_exists) {
        return true;
    }
    return false;
}
