<?php
/**
 * @package    report_cuattendance
 * @copyright  2016 Cornerstone University {@link http://www.cornerstone.edu}
 * @author	   Trevor McCready
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once(dirname(__FILE__) . '/locallib.php');

$id     = required_param('id', PARAM_INT);
$group  = optional_param('group', 0, PARAM_INT);
$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);

$page    = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);    // how many per page
$sort    = optional_param('sort', 'lastname', PARAM_ALPHA);
$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
$querystartdate = optional_param('querystartdate',null, PARAM_NOTAGS);
$queryenddate = optional_param('queryenddate',null, PARAM_NOTAGS);
$week = optional_param('week',null, PARAM_NOTAGS);
if(!is_null($week)) {
    $queryweek = json_decode($week,true);
    $querystartdate = $queryweek['start'];
    $queryenddate = strtotime(date_format_string($queryweek['end'],'%F').' 23:59:59');
} else {
    if(!is_null($querystartdate)) {
	    $querystartdate = strtotime($querystartdate);
    }
    if(!is_null($queryenddate)) {
	    $queryenddate = strtotime($queryenddate.' 23:59:59');
    }
}
require_login($course);
$context = context_course::instance($course->id);
require_capability('report/cuattendance:view', $context);
$PAGE->requires->js('/report/cuattendance/javascript/jquery.js',true);
$PAGE->requires->js('/report/cuattendance/javascript/jquery-ui.js', true);
$PAGE->requires->js('/report/cuattendance/javascript/cuattendance.js',true);
$PAGE->set_url('/report/cuattendance/index.php', array('id' => $id));
$returnurl = new moodle_url('/course/view.php', array('id' => $id));

// Determine if this is a PGS course or a SEM/TRD course
$school = '';
$sql = "SELECT cat.path
        FROM {course_categories} cat
        WHERE cat.id = $course->category";
$cat_record = $DB->get_record_sql($sql, array(), $strictness=IGNORE_MISSING);
$parent_categories = explode('/',$cat_record->path);
if ($parent_categories[1]) {
    $root_category = $parent_categories[1];
    $sql = "SELECT cat.name,cat.idnumber
            FROM {course_categories} cat
            WHERE cat.id = $root_category";
    $root_cat_record = $DB->get_record_sql($sql, array(), $strictness=IGNORE_MISSING);
    if ($root_cat_record->idnumber) {
        $school = $root_cat_record->idnumber;
    }
}

// Process the query
$userlist = get_enrolled_users($context, '', $group);
$suspended = get_suspended_userids($context);
$enrolledcount = count($userlist);
$suspendedcount = count($suspended);
$nonstudentsql = "SELECT COUNT(u.id)
			FROM {user} u
			JOIN {role_assignments} ra ON u.id = ra.userid
			JOIN {context} ctx ON ra.contextid = ctx.id
			JOIN {course} c ON c.id = ctx.instanceid
			WHERE ra.roleid = 3
			AND c.id = $COURSE->id
			GROUP BY u.id";
$nonstudents_exist = $DB->record_exists_sql($nonstudentsql);
if ( $nonstudents_exist ) {
    $nonstudentcount = $DB->count_records_sql($nonstudentsql, array());
} else {
    $nonstudentcount = 0;
}
$displaycount = $enrolledcount - $suspendedcount - $nonstudentcount;
// Determine and format course dates
if ($school == 'SEM' || $school == 'TRD') {
    $coursedayofweek = 'Saturday';
    $termbreakstart = 1583038800;
    $termbreakend = 1584600340;
} else {
    $coursedayofweek = date_format_string(strtotime('-1 days', $COURSE->startdate), '%A');
}
$courseduedates = 'Course Due/Meeting Dates: '.$coursedayofweek;

$lastcourseduedate = strtotime('last '.$coursedayofweek);
$coursestartdate = 'Course Start Date: '.date_format_string($COURSE->startdate,'%A %B %e, %Y',$USER->timezone);
if ( $school == 'SEM' || $school == 'TRD' ) {
    $weekdate[0] = strtotime(date_format_string($COURSE->startdate,'%B %e, %Y') . ' previous ' . $coursedayofweek);
} else {
    $courseopendate = strtotime('0 days', $COURSE->startdate);
    $weekdate[0] = strtotime('-1 days', $COURSE->startdate);
}
if ($COURSE->enddate) {
    $courseenddate = 'Course End Date: '.date_format_string($COURSE->enddate,'%A %B %e, %Y',$USER->timezone);
    if ( $school == 'SEM' || $school == 'TRD' ) {
        $lastdayofcourse = strtotime(date_format_string($COURSE->enddate,'%B %e, %Y') . ' previous ' . $coursedayofweek);
    } else {
        $lastdayofcourse = $COURSE->enddate;
    }
    $lengthofcourse = round(($lastdayofcourse - $weekdate[0])/604800+1);
} else {
    $courseenddate = "Course End Date: Not Set";
    $lastdayofcourse = $COURSE->startdate;
    if ($school == 'SEM' || $school == 'TRD' ) {
        $lengthofcourse = 15;
    } else {
        $lengthofcourse = 8; // Suggest we check on how many sections there are in the course and set the length of the course to that value if there is no end date set.
    }
}
if ($school == 'SEM' || $school == 'TRD' ) {
    for ($x = 1; $x < $lengthofcourse + 1; $x++) {
        if ( $x == $lengthofcourse ) {
            $weekdate[$x] = $COURSE->enddate;
        } elseif ( $x == 9 ) {
            $weekdate[$x] = strtotime('+8 days', $weekdate[$x-1]);
        } elseif ( $x == 10 ) {
            $weekdate[$x] = strtotime('+6 days', $weekdate[$x-1]);
        } else {
            $weekdate[$x] = strtotime('+7 days', $weekdate[$x-1]);
        }
        if ( $x == 1 ) {
            $weekstart[$x] = $COURSE->startdate;
        } else {
            $weekstart[$x] = strtotime('+1 day', $weekdate[$x-1]);
        }
        $weeks[$x] = 'Week '. $x . ' (' . date_format_string($weekstart[$x],'%F',$USER->timezone) . ' to ' . date_format_string($weekdate[$x],'%F',$USER->timezone) . ')';
    }
} else {
    for ($x = 1; $x < $lengthofcourse; $x++) {
	    $weekdate[$x] = strtotime('+7 days', $weekdate[$x-1]);
	    $weekstart[$x] = strtotime('-6 days', $weekdate[$x]);
	    $weeks[$x] = 'Week '. $x . ' (' . date_format_string($weekstart[$x],'%F',$USER->timezone) . ' to ' . date_format_string($weekdate[$x],'%F',$USER->timezone) . ')';
    }    
}
$courseinsession=true;

if ($lastcourseduedate <= $lastdayofcourse && $lastcourseduedate >= $weekstart[1]) {
    $lastweek = $z = 0;
	foreach ($weekdate as $classdate) {
		if ( $lastcourseduedate == $classdate ) {
			$lastweek = $z;
			break;
		} else {
			$z++;
		}
	}
	if (isset($lastweek)) {
        if(is_null($querystartdate)) {
			//$querystartdate = strtotime('2021-06-01 00:00:00');
            //echo '<pre>';
            //echo 'Last week: '.$lastweek.'<br/>';
            //echo 'Last course due date: '.$lastcourseduedate.'<br/>';
            //echo 'Length of course: '.$lengthofcourse.'<br/>';
            //echo 'Last day of course: '.$lastdayofcourse.'<br/>';
            //print_r($weekstart);
            //print_r($weekdate);
            //echo '</pre>';
            $querystartdate = $weekstart[$lastweek];
		}
		if(is_null($queryenddate)) {
            //$queryenddate = strtotime('2021-07-31 23:59:59');
			$queryenddate = strtotime(date_format_string($weekdate[$lastweek],'%F').' 23:59:59');
		}
	}
} else {
    $courseinsession=false;
	if (is_null($querystartdate) || $querystartdate == 1) {
		if($weekstart[1] >= strtotime("now")) {
			$querystartdate = strtotime(date('Y-01-01'));
		} else {
			$querystartdate = $courseopendate;
		}
	}
	if (is_null($queryenddate)) {
        $lastdayofcourse = strtotime(date_format_string($lastdayofcourse,'%F').' 23:59:59');
		$queryenddate = $lastdayofcourse;
	}
}
$datechoices = 'By default, the following date range was used: ' . date_format_string($querystartdate, '%F', $USER->timezone) . ' - ' . date_format_string($queryenddate, '%F', $USER->timezone);

// Prepare the column headings
$columns = array(	'firstname' => get_string('firstname'),
					'lastname'  => get_string('lastname'),
					'idnumber' 	=> get_string('idnumber'),
					'email'     => get_string('email'),
					'lastcourseaccess'=> get_string('lastcourseaccess', 'report_cuattendance'),
					'contributions'=> get_string('contributions', 'report_cuattendance'),
					'lastattendance'=> get_string('lastattendance', 'report_cuattendance'),
					'logdetails'   => get_string('logdetails', 'report_cuattendance'),
);
$hcolumns = array();
if (!isset($columns[$sort])) {
	$sort = 'lastname';
}
foreach ($columns as $column=>$strcolumn) {
	if ($sort != $column) {
		$columnicon = '';
		if ($column == 'lastmdlaccess') {
			$columndir = 'DESC';
		} else {
			$columndir = 'ASC';
		}
	} else {
		$columndir = $dir == 'ASC' ? 'DESC':'ASC';
		if ($column == 'lastcourseaccess') {
			$columnicon = $dir == 'ASC' ? 'up':'down';
		} else {
			$columnicon = $dir == 'ASC' ? 'down':'up';
		}
		$columnicon = " <img src=\"" . $OUTPUT->image_url('t/' . $columnicon) . "\" alt=\"\" />";
	}
	if ($column == 'logdetails' || $column == 'email') {
		$hcolumns[$column] = "$strcolumn";
	} else {
		$hcolumns[$column] = "<a href=\"index.php?id=$COURSE->id&amp;sort=$column&amp;dir=$columndir&amp;page=$page&amp;perpage=$perpage\">".$strcolumn."</a>$columnicon";
	}
}
$override = new stdClass();
$override->firstname = 'firstname';
$override->lastname = 'lastname';
$fullnamelanguage = get_string('fullnamedisplay', '', $override);
if (($CFG->fullnamedisplay == 'firstname lastname') or
		($CFG->fullnamedisplay == 'firstname') or
		($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'firstname lastname' )) {
			$fullnamedisplay = $hcolumns['firstname'].' / '.$hcolumns['lastname'];
		} else { // ($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'lastname firstname')
			$fullnamedisplay = $hcolumns['lastname'].' / '.$hcolumns['firstname'];
		}
$table = new html_table();
$table->head  = array($fullnamedisplay, $hcolumns['idnumber'], $hcolumns['email'], $hcolumns['lastcourseaccess'], $hcolumns['contributions'], $hcolumns['lastattendance'], $hcolumns['logdetails']);
$table->colclasses = array('leftalign name', 'leftalign idnumber', 'leftalign email', 'leftalign lastaccess', 'leftalign contributions', 'leftalign lastattendance', 'leftalign logdetails');
$table->id = 'cuattendance_contributions';
$table->attributes['class'] = 'generaltable';
$table->data  = array();
if ($sort == 'firstname' or $sort == 'lastname' or $sort == 'idnumber' or $sort == 'email') {
	$orderby = "u.$sort $dir";
} else {
	$orderby = "";
}
$mainuserfields = user_picture::fields('u');
if($group > 0) {
	$groupfilter = 'AND u.id in (SELECT gm.userid FROM {groups_members} gm WHERE gm.groupid = '.$group.')';
} else {
	$groupfilter = '';
}
$sql = "SELECT $mainuserfields, u.idnumber AS idnumber, FROM_UNIXTIME(ula.timeaccess) AS lastcourseaccess, COUNT(log.id) AS contributions, FROM_UNIXTIME(MAX(log.timecreated)) as lastattendance, u.suspended as suspended
		FROM {user} u
		JOIN {role_assignments} ra ON u.id = ra.userid
		JOIN {context} ctx ON ra.contextid = ctx.id
		JOIN {course} c ON c.id = ctx.instanceid
		JOIN {user_lastaccess} ula ON u.id = ula.userid	AND c.id = ula.courseid
		JOIN {enrol} e ON c.id = e.courseid
		JOIN {user_enrolments} ue ON u.id = ue.userid AND e.id = ue.enrolid
        LEFT JOIN {logstore_standard_log} log ON log.userid = u.id
        AND log.courseid = c.id
        AND log.edulevel = 2
        AND log.timecreated >= $querystartdate
        AND log.timecreated <= $queryenddate
        AND (
               (log.component LIKE 'assignsubmission%' AND log.action='submitted') 
            OR (log.component='mod_assign' AND log.action='submitted')
            OR (log.component='assignsubmission_file' AND log.action='uploaded' AND log.target='assessable')
            OR (log.component='mod_forum' AND log.action='created')
            OR (log.component='mod_hsuforum' AND log.action='created')
            OR (log.component='mod_quiz' AND log.action='submitted')
            OR (log.component='mod_turnitintooltwo' AND log.action='submission' AND log.target='add')
            )
        WHERE ra.roleid = 5
		AND ctx.instanceid = c.id
		AND c.id = $COURSE->id
		AND ue.status = 0
		$groupfilter
		GROUP BY u.id
		ORDER BY $orderby";
// log.component assignsubmission_file is checking merely against a file that a student has submitted to a dropbox. They have not necessarily submitted the assignment.
$records = $DB->get_recordset_sql($sql, array(), $page*$perpage, $perpage);
$recordcount = 0;
foreach ($records as $record) {
	$row = array();
	$row[] = html_writer::link(new moodle_url($CFG->wwwroot.'/user/view.php?id='.$record->id.'&course='.$COURSE->id),fullname($record));
	$row[] = $record->idnumber;
	$row[] = $record->email;
	$row[] = $record->lastcourseaccess;
	$row[] = html_writer::link(new moodle_url($CFG->wwwroot.'/report/log/index.php?chooselog=1&showuser=1&edulevel=2&id='.$COURSE->id.'&user='.$record->id.'&modaction=c'),$record->contributions,array('target'=>'moodlelog'));
	$row[] = $record->lastattendance;
	$row[] = html_writer::link(new moodle_url($CFG->wwwroot.'/report/log/index.php?chooselog=1&showuser=1&edulevel=2&id='.$COURSE->id.'&user='.$record->id),'View Log',array('target'=>'moodlelog'));
	$table->data[] = $row;
	++$recordcount;
}
$records->close();

$baseurl = new moodle_url('index.php', array('id' => $COURSE->id, 'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage));

// Display the results
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pagetitle', 'report_cuattendance').' for '.$COURSE->shortname);
echo html_writer::tag('h4',$courseduedates,array('id' => 'courseduedates', 'class' => 'legend'));
echo html_writer::tag('h4',$coursestartdate,array('id' => 'coursestartdate', 'class' => 'legend'));
echo html_writer::tag('h4',$courseenddate,array('id' => 'courseenddate', 'class' => 'legend'));
echo report_cuattendance_output_action_buttons($id, $group, $querystartdate,$queryenddate, 99, $PAGE->url,$weeks, $weekstart, $weekdate);
echo $OUTPUT->paging_bar($displaycount, $page, $perpage, $baseurl);
echo html_writer::table($table);
echo '<div id="cu_attendance_studentcount">';
if ($displaycount <= $perpage) {
	if ($displaycount > 0) {
		if ($displaycount > $recordcount) {
			echo '<span class="alert warn">Showing only students who have accessed the course; ';
		} else {
			echo '<span>Showing ';
		}
		echo $recordcount . ' of ' . $displaycount . ' enrolled students.</span>';
	} else {
		echo 'There are no enrolled students in this course, at this time.';
	}
	if ($suspendedcount > 0) {
		echo '<br/>' . $suspendedcount . ' suspended student(s) not included.';
	}
}
echo '</div>';
echo html_writer::link(new moodle_url($CFG->wwwroot.'/user/index.php?id='.$COURSE->id),'View Enrollments',array('class'=>'button'));
if (!$courseinsession) {
    $divtext = 'Notes: This ' . $school . ' course is not currently in session.';
    echo html_writer::tag('div',$divtext, array('style' => 'margin-top: 32px;'));
}
echo $OUTPUT->footer();
