<?php
/**
 * @package		report_cuattendance
 * @copyright	2016 Cornerstone University {@link http://www.cornerstone.edu}
 * @author 		Trevor McCready
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version	=	2020032600;
$plugin->requires	=	2018120300;
$plugin->release	=	'v1.0';
$plugin->maturity	=	MATURITY_BETA;
$plugin->component	=	'report_cuattendance';
