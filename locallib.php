<?php
/**
 * Local library functions for the CU Attendance report
 * 
 * @package    report_cuattendance
 * @copyright  2016 Cornerstone University {@link http://www.cornerstone.edu}
 * @author	   Trevor McCready
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function report_cuattendance_get_group_options($id) {
	$groupsfromdb = groups_get_all_groups($id);
	$groups = array();
	foreach ($groupsfromdb as $key => $value) {
		$groups[$key] = $value->name;
	}
	return $groups;
}
function report_cuattendance_output_action_buttons($id, $group, $querystart, $queryend, $timezone, $url, $weeks, $weekstart, $weekdate) {
	global $OUTPUT;
	$groups = report_cuattendance_get_group_options($id);
	$groupurl = clone $url;
	$querystartdate = date_format_string($querystart, '%F', $timezone);
	$queryenddate = date_format_string($queryend, '%F', $timezone);
	$select = new single_select($groupurl, 'group', $groups, $group, array('' => get_string('allusers', 'report_cuattendance')));
	$select->label = get_string('group');
	$html = html_writer::start_tag('div');
	$html .= $OUTPUT->render($select);
	$html .= html_writer::end_tag('div');
	$html .= html_writer::start_tag('div');
    $html .= html_writer::nonempty_tag('h5','Report Date Range');
	$html .= html_writer::start_tag('form',array('id'=>'cuattendancedatefilter','method'=>'get','action'=>$url,'style'=>'display: inline-block;'));
	$html .= html_writer::nonempty_tag('label','From ');
	$html .= html_writer::empty_tag('input',array('class'=>'ml-sm-1 mr-sm-1','type'=>'text', 'id'=>'querystartdate','name'=>'querystartdate', 'size'=>'10', 'value'=>$querystartdate, 'title'=>'0:00:00'));
	$html .= html_writer::nonempty_tag('label',' To ');
 	$html .= html_writer::empty_tag('input',array('class'=>'ml-sm-1','type'=>'text', 'id'=>'queryenddate','name'=>'queryenddate', 'size'=>'10', 'value'=>$queryenddate, 'title'=>'23:59:59'));
	$html .= html_writer::empty_tag('input',array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
	$html .= html_writer::empty_tag('input',array('type'=>'submit', 'name'=>'filter', 'value'=>'Filter'));
	$html .= html_writer::end_tag('form');
    $html .= html_writer::start_tag('form',array('name'=>'jump2week','method'=>'get','action'=>$url,'style'=>'display: inline-block; margin-left: 60px;'));
    $html .= html_writer::nonempty_tag('label','OR ');
    $html .= html_writer::start_tag('select',array('class'=>'ml-sm-1','id'=>'weekselect','name'=>'week', 'onChange'=>'document.forms[\'jump2week\'].submit();'));
    $html .= html_writer::nonempty_tag('option','Select a week... ',array('value'=>'','selected'=>''));
    $x=1;
    foreach ($weeks as $week) {
        $jsonvalues= json_encode(array('start'=>$weekstart[$x],'end'=>$weekdate[$x]));
        //$jsonvalues='{\'start\':\'' . $weekstart[$x] . '\',\'end\':\'' . $weekdate[$x] . '\'}';
        $html .= html_writer::nonempty_tag('option',$week,array('value'=>$jsonvalues));
        $x++;
    }
    $html .= html_writer::end_tag('select');
    $html .= html_writer::empty_tag('input',array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
    $html .= html_writer::end_tag('form');
	$html .= html_writer::end_tag('div');
	return $html;
}
